import { useEffect, useRef, useState } from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
// import './App.css'
import axios from "axios"
import Password from './component/Password'

import {NavLink, Route, Routes} from 'react-router-dom'
import So from './So'



function App() {
   return (        
        <div> 
         <p>hello world</p>
         {/* <NavLink to="/">home</NavLink>  */}
         <NavLink to="/">home</NavLink>
         <br />
         <NavLink to="/1/s">ids</NavLink>
         <br />
         <NavLink to="/1/s?name=sandeep">query string</NavLink>



         <Routes>

            <Route path="/" element={<div>i am inside home</div>}></Route>
            <Route path="/:id/s" element={<So></So>}></Route>
         </Routes>
          </div>          
       )
}

export default App
