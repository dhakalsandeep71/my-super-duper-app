import React, { useState } from 'react'

const Password = () => {
    let [password,setPassword]=useState(true)

  return (
    <div>
        Password:
        <input type={password ? "password" : "text"} />
        <button onClick={()=>{
            setPassword(!password)

        }}> {password ? "show " : "hide "}password</button>
    </div>
  )
}

export default Password