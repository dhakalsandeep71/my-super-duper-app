import React from 'react'
import { useParams, useSearchParams } from 'react-router-dom'

const So = () => {
    let a = useParams()
    let [searchParams] = useSearchParams()
  return (
    <div>
        {a.id}
        <br />
        {searchParams.get("name")}
    </div>
    
  )
}

export default So